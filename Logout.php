<?php
	session_start();
	
	//destruyendo la sesion
	session_destroy();
	
	//Redirigiendo al login
	header("Location: Login.php");
	exit;
?>