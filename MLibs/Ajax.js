//Creamos el objeto Ajax
//Medardo Alvarez 2011
function objetoAjax()
{
    var xmlhttp=false;
    try
    {
        xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
    }
    catch (e)
    {
        try {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
        catch (E)
            {
            xmlhttp = false;
            }
    }

    if (!xmlhttp && typeof XMLHttpRequest!='undefined')
    {
        xmlhttp = new XMLHttpRequest();
    }
    return xmlhttp;
}

//Validar Decimal
//onKeyPress="return ValidarDecimal(event)" 
function ValidarDecimal(e)
    {
    tecla = (document.all) ? e.keyCode : e.which;
	if (tecla==8 || tecla==9 ||  tecla==0) return true; // backspace and tab
        patron = /[0123456789.]/;
        te = String.fromCharCode(tecla);
        return patron.test(te);
    }

//Validar Numeros
//onKeyPress="return ValidarNumeros(event)" 
function ValidarNumeros(e)
    {
    tecla = (document.all) ? e.keyCode : e.which;
		if (tecla==8 || tecla==9 ||  tecla==0) return true; // backspace and tab		
        patron = /[0123456789]/;
        te = String.fromCharCode(tecla);
        return patron.test(te);
    }	
	
	
//VALIDAD CAMPOS	
function campos_requeridos(vector)
{	
	var error=0;
	var req = vector;
	for (var xv=0; xv<req.length; xv++)
	{   	
			if(document.getElementById(req[xv]).value=="")
			{
				document.getElementById(req[xv]).style.borderColor="red";			
				error++;
			}
			else
			{
				document.getElementById(req[xv]).style.borderColor="";			
			}
	}
	return error;
		
}
	/*
	Como usar la validacion
	var error = campos_requeridos(['competencias']);
	if(error>=1)
	{
			alert("Por favor complete todos los campos");
	}
	else
	{
	}
	*/



//Dar formato a hora hh:mm:ss
//onKeyPress="return FormatoHora(event,this)"	
function FormatoHora(evt,str) //Time format hh:mm:ss
{
var nav4 = window.Event ? true : false;
var key = nav4 ? evt.which : evt.keyCode;
hora=str.value; 		
if(hora.length==0)	{return ((key >= 48 && key <= 50));}
if(hora.length==1)	{if(hora.charAt(0)==2)	{return ((key >= 48 && key <= 51));}	else	{return ((key >= 48 && key <= 57));}}
if(hora.length==2)	{str.value=str.value+":"}
if(hora.length==3)	{return ((key >= 48 && key <= 53));}
if(hora.length==4)	{return ((key >= 48 && key <= 57));}
if(hora.length==5)	{str.value=str.value+":"}
if(hora.length==6)	{return ((key >= 48 && key <= 53));}
if(hora.length==7)	{return ((key >= 48 && key <= 57));}
if(hora.length> 7)	{return false;}
}

//Mostrar nombre de todos los elementos de un formulario
function ListFormElements(formid)
{
	var FormName = document.getElementById(formid);
    var FormElementName = ""; 							
    for (var i=0; i <= FormName.elements.length-1; i++) 
		{
		FormElementName += FormName.elements[i].name + ",";
		}
	document.write(FormElementName);
}

//Generar cadena con todos los elementos de un formulario
function StringFormElements(formid)
{
	var FormName = document.getElementById(formid);
    var StringForm = ""; 							
    var sepCampos = ""
    for (var i=0; i <= FormName.elements.length-1;i++) 
		{   	
        StringForm += sepCampos + FormName.elements[i].name+'='+encodeURIComponent(FormName.elements[i].value);     			
        sepCampos="&";
		}
	return StringForm;
} 


//_______________________________________ MOVER DIV
function P1_cargar(divID)
{
	posicion=0;
	if(navigator.userAgent.indexOf("MSIE")>=0){navegador="IE";}else{navegador="OTHER";}	//Verificamos navegador
	document.getElementById(divID).onmouseover=function(){ this.style.cursor="move";}
	document.getElementById(divID).ondblclick=P2_comienzoMovimiento;					//Con onmousedown llamamos la funcion para mover
	//document.getElementById(divID).onmousedown=P2_comienzoMovimiento;	
	
}

function evitaEventos(event)
{
	if(navegador=="IE"){window.event.cancelBubble=true;window.event.returnValue=false;}
	if(navegador=="OTHER"){event.preventDefault();}
}

function P2_comienzoMovimiento(event)
{
	var id=this.id;
	div_en_movimiento=document.getElementById(id);
	
	 // Obtengo la posicion del cursor
	if(navegador=="IE")
	 	{
	 	cursorComienzoX=window.event.clientX+document.documentElement.scrollLeft+document.body.scrollLeft;
		cursorComienzoY=window.event.clientY+document.documentElement.scrollTop+document.body.scrollTop;
		}
	if(navegador=="OTHER")
		{    
		cursorComienzoX=event.clientX+window.scrollX;
		cursorComienzoY=event.clientY+window.scrollY;
		}
	div_en_movimiento.onmousemove=P3_enMovimiento;
	div_en_movimiento.onmouseup=P4_finMovimiento;
	//div_en_movimiento.onmouseover=P4_finMovimiento;
	
	elComienzoX=parseInt(div_en_movimiento.style.left);
	elComienzoY=parseInt(div_en_movimiento.style.top);
	div_en_movimiento.style.zIndex=++posicion;				// Actualizo el posicion del elemento
	evitaEventos(event);									
}

function P3_enMovimiento(event)
{  
	var xActual, yActual;
	if(navegador=="IE")
	{    
		xActual=window.event.clientX+document.documentElement.scrollLeft+document.body.scrollLeft;
		yActual=window.event.clientY+document.documentElement.scrollTop+document.body.scrollTop;
	}  
	if(navegador=="OTHER")
	{
		xActual=event.clientX+window.scrollX;
		yActual=event.clientY+window.scrollY;
	}
	div_en_movimiento.style.left=(elComienzoX+xActual-cursorComienzoX)+"px";
	div_en_movimiento.style.top=(elComienzoY+yActual-cursorComienzoY)+"px";
	evitaEventos(event);
}

function P4_finMovimiento(event){div_en_movimiento.onmousemove=null;div_en_movimiento.onmouseup=null;}


function MostrarDiv(divID)
{
	document.getElementById(divID).style.display="inline";
	P1_cargar(divID);
}
function OcultararDiv(divID)
{
	document.getElementById(divID).style.display="none";
	P1_cargar(divID);
}



// _______________________________________ OCULTA DIV
function hideInfo(v1){document.getElementById(v1).style.display='none';} 

//_______________________________________ ILUMINAR CELDAS
function uno(src,color_entrada){src.bgColor=color_entrada;src.style.cursor="hand"; } 		
function dos(src,color_default){src.bgColor=color_default;src.style.cursor="default";} 

