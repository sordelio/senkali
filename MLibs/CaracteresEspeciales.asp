<%
'FUNCION QUE HARA QUE SE GUARDEN LOS CARACTERES ESPECIALES EN LA BASE TAL COMO LOS VEMOS
FUNCTION GuardarCaracteresEspeciales(ChangingChain)
	ChangingChain = Replace(ChangingChain,"á", "�")
	ChangingChain = Replace(ChangingChain,"��", "�")
	ChangingChain = Replace(ChangingChain,"é", "�")
	ChangingChain = Replace(ChangingChain,"�", "�")
	ChangingChain = Replace(ChangingChain,"��", "�")
	ChangingChain = Replace(ChangingChain,"ó", "�")
	ChangingChain = Replace(ChangingChain,"��", "�")	
	ChangingChain = Replace(ChangingChain,"��", "�")
	ChangingChain = Replace(ChangingChain,"ú", "�")
	ChangingChain = Replace(ChangingChain,"ñ", "�")
	ChangingChain = Replace(ChangingChain,"��", "�")
	ChangingChain = Replace(ChangingChain,"�", "�")
	ChangingChain = Replace(ChangingChain,"Ñ", "�")
	ChangingChain = Replace(ChangingChain,"'", " ")	
	ChangingChain = Replace(ChangingChain,"´", " ")	'----- Es el acento	
	ChangingChain = Replace(ChangingChain,"–", "�")			
	GuardarCaracteresEspeciales = ChangingChain	'Esto retornare		
END FUNCTION




FUNCTION MostrarCaracteresEspeciales2(ChangingChain)
	ChangingChain = Replace(ChangingChain,"Ñ", "&ntilde ")
	Response.Write(ChangingChain)
END FUNCTION


'FUNCION QUE HARA QUE SE MUESTREN LOS CARACTERES ESPECIALES
FUNCTION MostrarCaracteresEspeciales(cadena_a_convertir)

'Declaro las variables a usar
Dim letra_a_convertir 
Dim tamanho_de_cadena
Dim utftexto 

'Inicializo las variables a usar
utftexto = ""
cadena_a_convertir = Cstr(cadena_a_convertir)	'Tomo la informacion y la convierto en cadena
tamanho_de_cadena=Len(cadena_a_convertir)		'Calculo el tamanho de la cadena

FOR n = 1 To tamanho_de_cadena					'Lazo que recorrera la cadena

	letra_a_convertir = AscW(Mid(cadena_a_convertir, n, 1))		'Convierto la letra en codigo Ascii
	IF letra_a_convertir < 128 THEN
		utftexto = utftexto + Mid(cadena_a_convertir, n, 1)
	ELSEIF((letra_a_convertir > 127) And (letra_a_convertir < 2048)) THEN
		utftexto = utftexto + Chr(((letra_a_convertir \ 64) Or 192))
		utftexto = utftexto + Chr(((letra_a_convertir And 63) Or 128))
	ELSE
		utftexto = utftexto + Chr(((letra_a_convertir \ 144) Or 234))
		utftexto = utftexto + Chr((((letra_a_convertir \ 64) And 63) Or 128))
		utftexto = utftexto + Chr(((letra_a_convertir And 63) Or 128))
	END IF

NEXT

MostrarCaracteresEspeciales = utftexto			'Retorno la cadenas ya corregida
END FUNCTION


%>