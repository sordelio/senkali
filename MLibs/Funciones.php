<?php

function campana_base($campana)
{
	$camp = '';
	switch(strtoupper($campana)){
		case 'CHOICE HOTELS':
			$camp = 'choice';
		break;
		case 'AT&T':
			$camp = 'bellsouth';
		break;
		
		case 'BASE DE DATOS':
			$camp = 'bellsouth';
		break;
		
		case 'AT&T MOBILITY':
			$camp = 'mobility';
		break;
		case 'FEDEX':
			$camp = 'fedex';
		break;
        
        case 'ADMINISTRATIVOS':
            $camp = 'administrativos';
        break;
		
		case 'RRHH':
			$camp = 'fedex';
		break;
				
		case 'WFM':
			$camp = 'bellsouth';
		break;
		
		case 'KOHLS':
			$camp = 'kohls';
		break;
		case 'MOTOROLA':
			$camp = 'motorola';
		break;
		case 'SPRINT':
			$camp = 'sprint';
		break;
		case 'HILTON':
			$camp = 'hilton';
		break;
		case 'U-VERSE':
		case 'UVERSE':
			$camp = 'uverse';
		break;
		
		case 'CRICKET':
			$camp = 'aio';
		break;
		
		
		case 'IT':
			$camp = 'sistemas';
		break;
				
		//default:
			//$camp = 'tpslv';
	}

	return 	$camp;
}	


function  campana_sufijo($campana)
{
	$n = '';
	switch(strtoupper($campana)){
		case 'CHOICE HOTELS':
			$n = '';
			break;
		case 'AT&T':
			$n = '';
			break;
			
		case 'BASE DE DATOS':
			$n = '';
			break;


		case 'WFM':
			$n = '';
			break;
			
			
		case 'AT&T MOBILITY':
			$n = '';
			break;
			
		case 'FEDEX':
			$n = '1er_';
			break;
			


		case 'IT':
			$n = '1er_';
			break;
			
						
			
			case 'ADMINISTRATIVOS':
			$n = '1er_';
			break;
			
		
	
					case 'RRHH':
			$n = '1er_';
			break;
			
				
		case 'KOHLS':
			$n = '';
			break;
			
		case 'MOTOROLA':
			$n = '';
			break;
		case 'SPRINT':
			$n = '';
			break;
		case 'HILTON':
			$n = '';
		break;
		case 'U-VERSE':
			$n = '';
		break;
		
		case 'AIO':
			$n = '';
		break;
		
	//	default:
		//	$camp = 'tpslv';
	}
	return 	$n;
}




function  campana_sufijo2($campana)
{
	$n = '';
	switch(strtoupper($campana)){
		case 'CHOICE HOTELS':
			$n = '';
			break;
		case 'AT&T':
			$n = '';
			break;
			
		case 'BASE DE DATOS':
			$n = '';
			break;

		case 'WFM':
			$n = '';
			break;
			
		case 'AT&T MOBILITY':
			$n = '';
			break;
		case 'FEDEX':
			$n = '2do_';
			break;


		case 'IT':
			$n = '2do_';
			break;


			
					case 'ADMINISTRATIVOS':
			$n = '2do_';
			break;
			
			
			
		case 'KOHLS':
			$n = '';
			break;
		case 'MOTOROLA':
			$n = '';
			break;
		case 'SPRINT':
			$n = '';
			break;
		case 'HILTON':
			$n = '';
		break;
		case 'U-VERSE':
			$n = '';
		break;
		
		case 'AIO':
			$n = '';
		break;
		
	}
	return 	$n;
}

/********************************************* EVALUACIONES COACH ***************************************************/
function aplicarColores($prom){
	$css = '';
	if($prom <= 2.99){
   		$css = 'background-color:#F00; color:#FFF';
    }else if($prom >= 3 && $prom <= 3.99){ // ok
        $css = 'background-color:#FF0; font-weight:bold';
    }else if($prom >= 4 && $prom <= 4.49){
        $css = 'background-color:#060; color:#FFF';
     }else{
        $css = 'background-color:#036; color:#FFF';
     }
	 return $css;
}

function getPeriodo(){
	$mes = date('Y-m-d');
	$mes = substr($mes,5,2);
	if($mes>6)
		$periodo = 2;
	else
		$periodo = 1;
	
	return $periodo;	
}

function getEvRange($periodo,$base){
	if($base == 'calificaciones')
	{
		$anio = substr($periodo,2);
		$periodo = substr($periodo,0,1);
		if($periodo == 1)
		{
			$rango = " fecha_ingresada BETWEEN '".$anio."-01-01' AND '".$anio."-06-30'";
		}
		else
		{
			$rango = " fecha_ingresada BETWEEN '".$anio."-07-01' AND '".$anio."-12-31'";
		}
	}
	else
	{
		$anio = substr($periodo,2);
		$periodo = substr($periodo,0,1);
		if($periodo == 1)
		{
			$rango = " fecha_creacion BETWEEN '".$anio."-01-01' AND '".$anio."-06-30'";
		}
		else
		{
			$rango = " fecha_creacion BETWEEN '".$anio."-07-01' AND '".$anio."-12-31'";
		}	
	}	
	return $rango;
}

/********************************************* GENERAL INCIDENCIAS AP ***************************************************/

function sepFecha($fecha,$x){
	if(substr($fecha,2,1) != '-' && substr($fecha,2,1) != '/')
	{
		// Y-M-D
		// 2013-09-01
		switch($x)
		{
			case 'a':
				$anio = substr($fecha,0,4);
				return $anio;
			break;
			case 'm':
				$mes = substr($fecha,5,2);
				return $mes;
			break;
			case 'd':
				$dia = substr($fecha,8,2);
				return $dia;
			break;
		}
	}
	else
	{
		// D-M-Y
		// 01-09-2013
		switch($x)
		{
			case 'a':
				$anio = substr($fecha,6,4);
				return $anio;
			break;
			case 'm':
				$mes = substr($fecha,3,2);
				return $mes;
			break;
			case 'd':
				$dia = substr($fecha,0,2);
				return $dia;
			break;
		}
	}
}



function monthName($monthNum)
{
	$language = setlocale(LC_TIME,'Spanish');
	$monthName = ucwords(strftime('%B', mktime(0, 0, 0, $monthNum)));
	return $monthName;
}


function usaNuevasIncidencias($campana){
		$objConx = abrirconexion();
		$query = "SELECT incidencias as tipo FROM tpslv.campaign WHERE nombre_vista = '".$campana."' ";
		$result = consulta($query,$objConx);
		$result = mysql_fetch_array($result);
		$result = $result['tipo'];
		if($result == 'new')
			return true;
		else
			return false;
}

/*function monthName($monthNum)
{
	$language = setlocale(LC_TIME,'Spanish');
	$monthName = ucwords(strftime('%A', mktime(0, 0, 0, $monthNum)));
	return $monthName;
}*/

?>