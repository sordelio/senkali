<?php

	set_error_handler('MErrorHandler');
	
	function MErrorHandler($number,$text,$file,$line){
		if(ob_get_length())
			ob_clean();
			
		$style = "text-align:left; background-color:#FFB3B3; color:#E60000;
		border:1px solid red; font-family:Verdana;
		width:50%; margin:0 auto 0 auto;";
		$error='
			<div style="'.$style.'">
				<table style="color:red; text-align:left; margin:0 auto 0 auto;">
					<tr>
						<th colspan="2" style="text-align:center;">Error '.$number.'</th>
					</tr>
					<tr>
						<td>Message:&nbsp;&nbsp;</td>
						<td>'.$text.'</td>
					</tr>
					<tr>
						<td>File:&nbsp;&nbsp;</td>
						<td>'.$file.'</td>
					</tr>
					<tr>
						<td>Line:&nbsp;&nbsp;</td>
						<td>'.$line.'</td>
					</tr>
				</table>
			</div>
			<br>
		';
		
		echo $error;
		exit;
	}
	

?>