<?php

	/*NECESARIOS PARA EL ENVIO DE CORREO*/
	include("includes/class.phpmailer.php");
	include("includes/class.smtp.php");
	
	/*FUNCION PARA MOSTRAR AÑO EN SELECT*/
	
	function anio($anioinicial, $aniofinal)
		{   
  			$anhoa_ctual=Date('Y'); 
			for ($anho = $anioinicial; $anho <= $aniofinal ; $anho++) 
			{
			if($anho==$anhoa_ctual)
			{
				$sel="selected='selected'";
			}
			else
			{
				$sel="";			
			}
			
			echo("<option value=".$anho." ".$sel.">".$anho."</option>");

			}
		  
		}
		
		
		/*FUNCION PARA MOSTRAR MES EN SELECT*/
	function mes()
		{
		
		$meses =array("JANUARY","FEBRUARY","MARCH","APRIL","MAY","JUNE","JULY","AUGUST","SEPTEMBER","OCTOBER","NOVEMBER","DECEMBER");
		
			for ($i = 0; $i <12 ; $i++) 
			{
				$dias=$i+1;
				if($dias<10){$dias="0".$dias;}
				if($dias==Date('m'))
				{
					$seldias="selected='selected'";
				}
				else
				{
					$seldias="";			
				}			
	
				echo("<option value=".$dias." ".$seldias.">".$meses[$i]."</option>");
			
			}
		}
		/*FUNCION PARA DIFERENCIA DE FECHA*/
		function diferenciafecha($date1,$date2,$valor)
		{
		
			$desde = new DateTime($date1);
			$hasta = new DateTime($date2);
			$interval = $desde->diff($hasta);
			$retorno=strtoupper($valor);
			if($retorno=="Y"){ return $interval->y;}else if($retorno=="M"){return $interval->m;}else if($retorno=="D"){return $interval->d;}
		}
		
		
		/*FUNCION PARA ENVIAR CORREO*/
		function enviarcorreo($subject,$cuerpo,$remitentes,$copia,$copiaoculta,$mensaje)
		{
			date_default_timezone_set("America/Costa_Rica" ) ;
	
			$mail = new PHPMailer();
			$mail -> IsSMTP();
			$mail -> SetLanguage( 'en', 'phpmailer/language/' );
	        $mail -> Host = 'pop.tpslv.com';
	        $mail -> Port = 25;
	        $mail -> SMTPAuth = true;
			$mail->Username = "myinfo@tpslv.com";
			$mail->Password = "Myinfo001";
			$mail->From = "no-reply@tpslv.com";
	        $mail->FromName = "no-reply@tpslv.com";
			$mail->Subject =$subject;
			$mail->Body =$cuerpo;
			
			$quienes = explode(',',$remitentes);
			$quienescopia=explode(',',$copia);
			$quienesoculto=explode(',',$copiaoculta);
			
			
			if($remitentes!="")
			{			
				foreach($quienes as $destinatarios)
				{
					$mail->AddAddress($destinatarios);
				}
			}
				
				
			if($copia!="")
				{

						foreach($quienescopia as $copias)
							{
								$mail->AddCC($copias);
							}
				}
				
				if($copiaoculta!="")
				{
						foreach($quienesoculto as $copiaoculta)
							{
								$mail->AddBCC($copiaoculta);
							}
				}

			
			
			$mail->IsHTML(true);
			$exito=$mail->Send();
			
		         if(!$exito)
				 {

					// echo $mensaje."<br/>". $mail->ErrorInfo ;
					 
					if ($_SERVER["HTTPS"] == "on") 
					{
						$protocolo = "https://";
					}
					else
					{
						$protocolo = "http://";	
					}					
					
					$urlactual = $_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF'];
					$llpedazos=explode("/",$urlactual);
					$cadenaruta=$protocolo.$llpedazos[0]."/".$llpedazos[1]."/MLibs/php/error.php";
					
					 
					echo "<form name='fincidenciaerror' method='post' action='".$cadenaruta."'>";
					echo "<input type='hidden' id='mensaje' name='mensaje' value='".$mensaje."'  />";
					echo "<input type='hidden' id='descrt' name='descrt' value='".$mail->ErrorInfo."' />";
					echo "</form>";
											
					echo "<script language = 'Javascript'>";
					echo "document.fincidenciaerror.target='fincidenciaerror';";
					echo "window.open('','fincidenciaerror','toolbar=0,scrollbars=yes,location=0,statusbar=0,menubar=0,resizable=yes,width=560,height=400');";
					echo "document.fincidenciaerror.submit();";
					echo "</script>";

		      	 }
				 
				 
			
		
		}
			
			
function distrocorreo($app, $super, $jp, $gc)
{

$datosv="";
$conexion=abrirconexion();
$consultda="SELECT * FROM tpslv.correo_aplicaciones where id='".$app."'";	
$data=consulta($consultda,$conexion);
	while($dato=mysql_fetch_array($data))
	{
		//BUSCO EL CORREO DEL SUPERVISOR
		if($super!="" &&  $dato["supervisor"]=="1" )
		{
			$consultdad="SELECT correo FROM ".$dato["campana"].".agentes where nomina='".$super."' and status='1' ";	
			$data=consulta($consultdad,$conexion);
			
			if (mysql_num_rows($data)>0)
			{	
				$datod=mysql_fetch_array($data);
				$correosup=$datod["correo"].", ";
			}
			else
			{
				$correosup="";
			}
		}
		else
		{
			$correosup="";
		}
		
		
		
		
		//BUSCO EL CORREO DEL JEFE DE PISO
		if($jp!="" &&  $dato["jefepiso"]=="1" )
		{
			$consultdad="SELECT correo FROM ".$dato["campana"].".agentes where nomina='".$jp."' and status='1' ";	
			$data=consulta($consultdad,$conexion);

			if (mysql_num_rows($data)>0)
			{	
				$datod=mysql_fetch_array($data);
				$correojp=$datod["correo"].", ";
			}
			else
			{
				$correojp="";
			}
					
		}
		else
		{
			$correojp="";
		}
		
		//BUSCO EL CORREO DEL GERENTE DE CUENTA		
		if($gc!="" &&  $dato["gerente_cuenta"]=="1" )
		{
			$consultdad="SELECT correo FROM ".$dato["campana"].".agentes where nomina='".$gc."' and status='1' ";	
			$data=consulta($consultdad,$conexion);

			if (mysql_num_rows($data)>0)
			{	
				$datod=mysql_fetch_array($data);
				$correogc=$datod["correo"].", ";
			}
			else
			{
				$correogc="";
			}	
			
		}
		else
		{
			$correogc="";
		}		
		
		$remitentestodos=$correosup.$correojp.$correogc.$dato["remitentes"];
			
		
	$datosv[] = array('remitentes' => $remitentestodos,'copia' => $dato["copia"],'copia_oculta' => $dato["copia_oculta"]);

}
return $datosv;
cerrarconexion($conexion);
}			
			
?>		